# slack-event-processing

Simple python app that listens to slack EventsAPI and stores things in a MariaDB.

## Setup

First you'll need to have a Slack Bot set up to use the Slack API:

1. [https://api.slack.com/start/building/bolt-python#create](Create a Slack App on the Slack API system.)
2. Enable the following scopes:
    1. TODO: list the scopes used/needed
3. Install your App on your Slack servers.

After cloning this repo:

1. Set up a venv
    1. `python3 -m venv .venv`
    2. `source .venv/bin/activate`
2. Install the requirements with `pip install -r requirements.txt`
3. Set up your Environment Variables with `envsetup.sh` as described below.

### Environment

This app relies on having your Slack Bot and Maria DB credentials in enviornment variables.
`cp envsetup_template.sh envsetup.sh` and then edit your `envsetup.sh`, inserting your credentials. `source` your `envsetup.sh` in your venv to set the envvars.

## Usage

Simply run `app.py` in your venv.

If you need to pass in any extra arguments for mariadb, put them on the commandline as kwargs, e.g. `ssl_key=path/to/private/key/file` or anything else used by [ConnectionPool](https://mariadb-corporation.github.io/mariadb-connector-python/module.html#mariadb.ConnectionPool) or [connect](https://mariadb-corporation.github.io/mariadb-connector-python/module.html#mariadb.connect)

## License

MIT
