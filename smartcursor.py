import logging
from typing import Sequence

import mariadb
from mariadb_composition import Sql
from tenacity import retry, retry_if_exception_type, wait_random_exponential

logger = logging.getLogger()


class SmartCursor(object):
    """
    Wrapper of mariadb.Cursor that self-manages its own connection from the pool.
    It supports all of the methods of mariadb.Cursor that make sense to support from mariaDB 1.1.10
    """

    _cur: mariadb.Cursor
    _conn: mariadb.Connection

    @retry(
        wait=wait_random_exponential(multiplier=1, max=300),
        retry=retry_if_exception_type(mariadb.PoolError),
    )
    def _getConnection(self, pool: mariadb.ConnectionPool):
        return pool.get_connection()

    def __init__(self, pool: mariadb.ConnectionPool, useDB: str | None = None):
        self._conn = self._getConnection(pool)
        self._conn.autocommit = True
        self._cur = self._conn.cursor()
        if useDB:
            self._cur.execute(f"USE {useDB}")

    def __del__(self):
        self._cur.close()
        self._conn.close()

    def check_closed(self):  # -> None:
        self._cur.check_closed()

    def callproc(self, sp: str, data: Sequence = ()):  # -> None:
        """
        Executes a stored procedure sp. The data sequence must contain an
        entry for each parameter the procedure expects.

        Input/Output or Output parameters have to be retrieved by .fetch
        methods, the .sp_outparams attribute indicates if the result set
        contains output parameters.

        Arguments:
            - sp: Name of stored procedure.
            - data: Optional sequence containing data for placeholder
                    substitution.
        """
        self._cur.callproc(sp, data)

    def nextset(self):
        """
        Will make the cursor skip to the next available result set,
        discarding any remaining rows from the current set.
        """
        return self._cur.nextset()

    def execute(
        self, statement: Sql.Composed, data: Sequence | dict = (), buffered=None
    ):  # -> None:
        """
        Prepare and execute a SQL statement.

        Parameters may be provided as sequence or mapping and will be bound
        to variables in the operation. Variables are specified as question
        marks (paramstyle ='qmark'), however for compatibility reasons MariaDB
        Connector/Python also supports the 'format' and 'pyformat' paramstyles
        with the restriction, that different paramstyles can't be mixed within
        a statement.

        A reference to the operation will be retained by the cursor.
        If the cursor was created with attribute prepared =True the statement
        string for following execute operations will be ignored.
        This is most effective for algorithms where the same operation is used,
        but different parameters are bound to it (many times).

        By default execute() method generates an buffered result unless the
        optional parameter buffered was set to False or the cursor was
        generated as an unbuffered cursor.
        """
        stmt = statement.as_string()
        logger.debug(f"statement:\n\t{stmt}\ndata:\n\t{data}")
        self._cur.execute(stmt, data, buffered)

    def safe_execute(
        self,
        statement: Sql.Composed,
        data: Sequence | dict = (),
        buffered=None,
        logLevel=logging.WARN,
    ) -> bool:
        """
        Wrap execute() to handle vexing exceptions.
        """
        stmt = statement.as_string()
        logger.debug(f"statement:\n\t{stmt}\ndata:\n\t{data}")
        try:
            self._cur.execute(stmt, data, buffered)
            return True
        except mariadb.Error as e:
            logger.log(logLevel, f"Error executing statement '{stmt}': {e}")
            return False

    def executemany(self, statement: Sql.Composed, parameters):  # -> None:
        """
        Prepare a database operation (INSERT,UPDATE,REPLACE or DELETE
        statement) and execute it against all parameter found in sequence.

        Exactly behaves like .execute() but accepts a list of tuples, where
        each tuple represents data of a row within a table.
        .executemany() only supports DML (insert, update, delete) statements.

        If the SQL statement contains a RETURNING clause, executemany()
        returns a result set containing the values for columns listed in the
        RETURNING clause.
        """
        stmt = statement.as_string()
        logger.debug("executemany statement:\n\t" + stmt)
        self._cur.executemany(stmt, parameters)

    def safe_executemany(
        self,
        statement: Sql.Composed,
        parameters,
        logLevel=logging.WARN,
    ) -> bool:
        """
        Wrap executemany() to handle vexing exceptions.
        """
        stmt = statement.as_string()
        logger.debug("safe_executemany statement:\n\t" + stmt)
        try:
            self._cur.executemany(stmt, parameters)
            return True
        except mariadb.Error as e:
            logger.log(logLevel, f"Error executemany-ing statement '{stmt}': {e}")
            return False

    def close(self):  # -> None:
        """
        Closes the cursor.

        If the cursor has pending or unread results, .close() will cancel them
        so that further operations using the same connection can be executed.

        The cursor will be unusable from this point forward; an Error
        (or subclass) exception will be raised if any operation is attempted
        with the cursor."
        """
        self._cur.close()

    def fetchone(self):
        """
        Fetch the next row of a query result set, returning a single sequence,
        or None if no more data is available.

        An exception will be raised if the previous call to execute() didn't
        produce a result set or execute() wasn't called before.
        """
        return self._cur.fetchone()

    def safe_fetchone(self, logLevel=logging.WARN):
        """
        Wrap the mariaDB fetchone() to handle vexing exceptions.
        """
        try:
            return self._cur.fetchone()
        except mariadb.Error as e:
            logger.log(logLevel, f"Error fetchone: {e}")
            return None

    def fetchmany(self, size: int = 0):
        """
        Fetch the next set of rows of a query result, returning a sequence
        of sequences (e.g. a list of tuples). An empty sequence is returned
        when no more rows are available.

        The number of rows to fetch per call is specified by the parameter.
        If it is not given, the cursor's arraysize determines the number
        of rows to be fetched. The method should try to fetch as many rows
        as indicated by the size parameter.
        If this is not possible due to the specified number of rows not being
        available, fewer rows may be returned.

        An exception will be raised if the previous call to execute() didn't
        produce a result set or execute() wasn't called before.
        """
        return self._cur.fetchmany(size)

    def fetchall(self):
        """
        Fetch all remaining rows of a query result, returning them as a
        sequence of sequences (e.g. a list of tuples).

        An exception will be raised if the previous call to execute() didn't
        produce a result set or execute() wasn't called before.
        """
        return self._cur.fetchall()

    def safe_fetchall(self, logLevel=logging.WARN):
        """
        Wrap the mariaDB fetchall() to handle vexing exceptions.
        """
        try:
            return self._cur.fetchall()
        except mariadb.Error as e:
            logger.log(logLevel, f"Error fetchall: {e}")
            return None

    def __iter__(self):
        return iter(self.fetchone, None)

    def scroll(self, value: int, mode="relative"):  # -> None:
        """
        Scroll the cursor in the result set to a new position according to
        mode.

        If mode is "relative" (default), value is taken as offset to the
        current position in the result set, if set to absolute, value states
        an absolute target position.
        """
        self._cur.scroll(value, mode)

    def setinputsizes(self, size: int):  # -> None:
        """
        Required by PEP-249. Does nothing in MariaDB Connector/Python
        """
        return

    def setoutputsize(self, size: int):  # -> None:
        """
        Required by PEP-249. Does nothing in MariaDB Connector/Python
        """
        return

    @property
    def rowcount(self) -> int:
        """
        This read-only attribute specifies the number of rows that the last\
        execute*() produced (for DQL statements like SELECT) or affected
        (for DML statements like UPDATE or INSERT).
        The return value is -1 in case no .execute*() has been performed
        on the cursor or the rowcount of the last operation  cannot be
        determined by the interface.
        """
        return self._cur.rowcount()

    @property
    def sp_outparams(self) -> bool:
        """
        Indicates if the current result set contains in out or out parameter
        from a previous executed stored procedure
        """
        return self._cur.sp_outparams()

    @property
    def lastrowid(self):
        """
        Returns the ID generated by a query on a table with a column having
        the AUTO_INCREMENT attribute or the value for the last usage of
        LAST_INSERT_ID().

        If the last query wasn't an INSERT or UPDATE
        statement or if the modified table does not have a column with the
        AUTO_INCREMENT attribute and LAST_INSERT_ID was not used, the returned
        value will be None
        """
        return self._cur.lastrowid()
