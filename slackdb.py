import logging
import sys
from asyncio import Task
from enum import IntEnum, auto
from os import environ as osenviron
from typing import Any

import mariadb
from mariadb.connectionpool import ConnectionPool
from mariadb_composition import Sql
from slack_sdk.errors import SlackApiError
from slack_sdk.web.async_client import AsyncWebClient
from slack_sdk.web.async_slack_response import AsyncSlackResponse

from smartcursor import SmartCursor

logger = logging.getLogger()

STALE_THRESHHOLD = Sql.Literal(
    24 * 60 * 60  # One day.
)  # Threshhold in Seconds to update info from the Slack API.

LASTUPDATE_KEY = Sql.Identifier("last_update")
LASTUPDATE_DEFINITION = Sql.SQL(
    "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"
)


class users(object):
    NAME = Sql.Identifier("users")

    class keys(object):
        ID = Sql.Identifier("user")
        NAME = Sql.Identifier("name")
        REALNAME = Sql.Identifier("real_name")
        PROFILE_DISPLAYNAME = Sql.Identifier("profile_display_name")
        PROFILE_EMAIL = Sql.Identifier("profile_email")
        PROFILE_REALNAME = Sql.Identifier("profile_real_name")

    class defs(object):
        ID = Sql.SQL("VARCHAR(30)")
        NAME = Sql.SQL("VARCHAR(21)")
        REALNAME = Sql.SQL("TEXT")
        PROFILE_DISPLAYNAME = Sql.SQL("TEXT")
        PROFILE_EMAIL = Sql.SQL("TEXT")
        PROFILE_REALNAME = Sql.SQL("TEXT")

    CREATE = Sql.createTable(
        NAME,
        (
            LASTUPDATE_KEY + LASTUPDATE_DEFINITION,
            keys.ID + defs.ID + Sql.PRIMARY,
            keys.NAME + defs.NAME + Sql.NULL,
            keys.REALNAME + defs.REALNAME + Sql.NULL,
            keys.PROFILE_REALNAME + defs.PROFILE_REALNAME + Sql.NULL,
            keys.PROFILE_DISPLAYNAME + defs.PROFILE_DISPLAYNAME + Sql.NULL,
            keys.PROFILE_EMAIL + defs.PROFILE_EMAIL + Sql.NULL,
        ),
    )


class channels(object):
    NAME = Sql.Identifier("channels")

    class keys(object):
        ID = Sql.Identifier("channel")
        NAME = Sql.Identifier("name")
        CREATED = Sql.Identifier("created")
        CREATOR = Sql.Identifier("creator")
        ISPRIVATE = Sql.Identifier("is_private")
        UPDATED = Sql.Identifier("updated")
        TOPIC = Sql.Identifier("topic")
        TOPIC_CREATOR = Sql.Identifier("topic_creator")
        TOPIC_LAST_SET = Sql.Identifier("topic_last_set")
        PURPOSE = Sql.Identifier("purpose")
        PURPOSE_CREATOR = Sql.Identifier("purpose_creator")
        PURPOSE_LAST_SET = Sql.Identifier("purpose_last_set")

    class defs(object):
        ID = Sql.SQL("VARCHAR(30)")
        NAME = Sql.SQL("TEXT")
        CREATED = Sql.SQL("INT UNSIGNED")
        CREATOR = users.defs.ID
        ISPRIVATE = Sql.SQL("BOOL")
        UPDATED = Sql.SQL("BIGINT UNSIGNED")
        TOPIC = Sql.SQL("TEXT")
        TOPIC_CREATOR = users.defs.ID
        TOPIC_LAST_SET = CREATED
        PURPOSE = Sql.SQL("TEXT")
        PURPOSE_CREATOR = users.defs.ID
        PURPOSE_LAST_SET = CREATED

    CREATE = Sql.createTable(
        NAME,
        (
            LASTUPDATE_KEY + LASTUPDATE_DEFINITION,
            keys.ID + defs.ID + Sql.PRIMARY,
            keys.NAME + defs.NAME + Sql.NONNULL,
            keys.CREATED + defs.CREATED + Sql.NONNULL,
            keys.CREATOR + defs.CREATOR + Sql.NONNULL,
            keys.ISPRIVATE + defs.ISPRIVATE + Sql.NULL,
            keys.UPDATED + defs.UPDATED + Sql.NONNULL,
            keys.TOPIC + defs.TOPIC + Sql.NULL,
            keys.TOPIC_CREATOR + defs.TOPIC_CREATOR + Sql.NULL,
            keys.TOPIC_LAST_SET + defs.TOPIC_LAST_SET + Sql.NULL,
            keys.PURPOSE + defs.PURPOSE + Sql.NULL,
            keys.PURPOSE_CREATOR + defs.PURPOSE_CREATOR + Sql.NULL,
            keys.PURPOSE_LAST_SET + defs.PURPOSE_LAST_SET + Sql.NULL,
            Sql.foreignKey(
                Sql.Identifier("fk_creator"), keys.CREATOR, users.NAME, users.keys.ID
            ),
            Sql.foreignKey(
                Sql.Identifier("fk_topic_creator"),
                keys.TOPIC_CREATOR,
                users.NAME,
                users.keys.ID,
            ),
            Sql.foreignKey(
                Sql.Identifier("fk_purpose_creator"),
                keys.PURPOSE_CREATOR,
                users.NAME,
                users.keys.ID,
            ),
        ),
    )


class messages(object):
    NAME = Sql.Identifier("messages")

    class keys(object):
        BLOCKS = Sql.Identifier("blocks")
        TEXT = Sql.Identifier("text")
        TS = Sql.Identifier("ts")
        THREAD_TS = Sql.Identifier("thread_ts")
        USERID = users.keys.ID
        TEAM = Sql.Identifier("team")
        CHANNEL = channels.keys.ID
        DELETED_BY = Sql.Identifier("deleted_by")
        DELETED_AT = Sql.Identifier("deleted_at")
        CHANGED_BY = Sql.Identifier("changed_by")
        CHANGED_AT = Sql.Identifier("changed_at")

    class defs(object):
        BLOCKS = Sql.SQL("TEXT")
        TEXT = Sql.SQL("TEXT")
        TS = Sql.SQL("DECIMAL(16,6) UNSIGNED")
        THREAD_TS = TS
        USERID = users.defs.ID
        TEAM = USERID
        CHANNEL = channels.defs.ID
        DELETED_BY = USERID
        DELETED_AT = TS
        CHANGED_BY = USERID
        CHANGED_AT = TS

    CREATE = Sql.createTable(
        NAME,
        (
            keys.TS + defs.TS + Sql.PRIMARY,
            keys.USERID + defs.USERID + Sql.NONNULL,
            keys.TEAM + defs.TEAM + Sql.NULL,
            keys.CHANNEL + defs.CHANNEL + Sql.NONNULL,
            keys.THREAD_TS + defs.THREAD_TS + Sql.NULL,
            keys.TEXT + defs.TEXT + Sql.NONNULL,
            keys.BLOCKS + defs.BLOCKS + Sql.NONNULL,
            keys.DELETED_BY + defs.DELETED_BY + Sql.NULL,
            keys.DELETED_AT + defs.DELETED_AT + Sql.NULL,
            keys.CHANGED_BY + defs.CHANGED_BY + Sql.NULL,
            keys.CHANGED_AT + defs.CHANGED_AT + Sql.NULL,
            Sql.foreignKey(
                Sql.Identifier("fk_userid"), keys.USERID, users.NAME, users.keys.ID
            ),
            Sql.foreignKey(
                Sql.Identifier("fk_channel"),
                keys.CHANNEL,
                channels.NAME,
                channels.keys.ID,
            ),
            Sql.foreignKey(
                Sql.Identifier("fk_deleted_by"),
                keys.DELETED_BY,
                users.NAME,
                users.keys.ID,
            ),
            Sql.foreignKey(
                Sql.Identifier("fk_changed_by"),
                keys.CHANGED_BY,
                users.NAME,
                users.keys.ID,
            ),
        ),
    )


class messagechanges(object):
    NAME = Sql.Identifier("messagechanges")

    class keys(object):
        CHANGED_AT = messages.keys.CHANGED_AT
        TS = messages.keys.TS
        CHANGED_BY = messages.keys.CHANGED_BY
        PREVIOUS_MESSAGE = Sql.Identifier("previous_message")

    class defs(object):
        CHANGED_AT = messages.defs.CHANGED_AT
        TS = messages.defs.TS
        CHANGED_BY = messages.defs.CHANGED_BY
        PREVIOUS_MESSAGE = Sql.SQL("TEXT")

    CREATE = Sql.createTable(
        NAME,
        (
            keys.CHANGED_AT + defs.CHANGED_AT + Sql.PRIMARY,
            keys.TS + defs.TS + Sql.NONNULL,
            keys.CHANGED_BY + defs.CHANGED_BY + Sql.NONNULL,
            keys.PREVIOUS_MESSAGE + defs.PREVIOUS_MESSAGE + Sql.NONNULL,
            Sql.foreignKey(
                Sql.Identifier("fk_ts"), keys.TS, messages.NAME, messages.keys.TS
            ),
        ),
    )


class SlackDB(object):
    """
    SlackDB is a Singleton class used to store and update a MariaDB backup of Slack message data.
    It wraps a mariadb.ConnectionPool to read and write to the DB and needs a Slack AsyncWebClient to make requests as needed.
    """

    _pool: ConnectionPool
    _slack: AsyncWebClient
    _dbname: Sql.Identifier

    def __new__(cls, *args, slackClient, **kwargs):
        if not hasattr(cls, "instance"):
            cls.instance = super(SlackDB, cls).__new__(cls)
        return cls.instance

    def __init__(self, *args, slackClient: AsyncWebClient, **kwargs):
        """
        Initialize the instance with the mariadb.ConnectionPool args, passing whatever
        we get through to it.
        """
        try:
            self._slack = slackClient
            self._pool = mariadb.ConnectionPool(*args, **kwargs)
        except mariadb.Error as e:
            logger.error(f"Error connecting to MariaDB Platform: {e}")
            sys.exit(1)

        try:
            dbname = osenviron["MARIADB_DATABASE"]
        except KeyError:
            dbname = "slack"
        self._dbname = Sql.Identifier(dbname)

        cur = SmartCursor(self._pool)
        if not (
            cur.safe_execute(
                Sql.SQL("CREATE DATABASE IF NOT EXISTS {}").format(self._dbname),
                logLevel=logging.ERROR,
            )
            and cur.safe_execute(
                Sql.SQL("USE {}").format(self._dbname), logLevel=logging.ERROR
            )
        ):
            sys.exit(1)

        if not cur.safe_execute(users.CREATE, logLevel=logging.ERROR):
            sys.exit(1)

        if not cur.safe_execute(channels.CREATE, logLevel=logging.ERROR):
            sys.exit(1)

        if not cur.safe_execute(messages.CREATE, logLevel=logging.ERROR):
            sys.exit(1)

        if not cur.safe_execute(messagechanges.CREATE, logLevel=logging.ERROR):
            sys.exit(1)

    def _removeNulls(self, d: dict):
        keys = list(d.keys())
        for k in keys:
            v = d[k]
            if not v or "NULL" == v:
                d.pop(k)
        return d

    class _SQLReturn(IntEnum):
        INSERT = auto()
        UPDATE = auto()
        BREAK = auto()

    async def _unvexCall(self, func, *args, **kwargs) -> AsyncSlackResponse | None:
        """
        The Slack API uses vexing exceptions in many places.
        Wrap a slack api function to catch the error.
        """
        try:
            response = await func(*args, **kwargs)
        except SlackApiError as e:
            logger.info(e)
            return None
        return response

    @classmethod
    def checkResultCallback(cls, t: Task[bool]):
        if t.result():
            logger.info("Task success")
        else:
            logger.error("Task failed")

    async def updateUser(
        self, userId: str | None, cur: SmartCursor | None = None
    ) -> bool:
        """
        Insert userId to users.NAME, updating existing data with slack
        response if needed.
        """

        if not userId or userId == "NULL":
            # ignore null users
            return True

        botId = None
        botInfo = {}
        if userId[0] == "B":
            # We have a bot user; convert the bot_id into a user_id
            botId = userId
            response = await self._unvexCall(self._slack.bots_info, bot=userId)
            if not response:
                logger.debug("Response was bad")
                return False
            botInfo = response["bot"]
            userId = botInfo.get("user_id")

        if not cur:
            cur = SmartCursor(self._pool, self._dbname.as_string())

        # Determinw what kind of action to take:
        #  1. INSERT if userid isn't in the table.
        #  2. REPLACE if userid is in the table, but stale.
        #  3. BREAK  if userid is in the table, but not stale.
        query = Sql.SQL(
            "SELECT IF( NOT EXISTS ( SELECT 1 FROM {table} WHERE {usridkey}={usrid} ), {ins}, IF( EXISTS ( SELECT 1 FROM {table} WHERE {usridkey}='{usrid}' AND {lastupdate} < DATE_SUB( NOW(), INTERVAL {threshhold} SECOND ) ), {upd}, {brk} ) )"
        ).format(
            table=users.NAME,
            usridkey=users.keys.ID,
            usrid=Sql.Placeholder("usrid"),
            lastupdate=LASTUPDATE_KEY,
            threshhold=STALE_THRESHHOLD,
            ins=Sql.Literal(self._SQLReturn.INSERT),
            upd=Sql.Literal(self._SQLReturn.UPDATE),
            brk=Sql.Literal(self._SQLReturn.BREAK),
        )
        if not cur.safe_execute(query, {"usrid": botId or userId}):
            logger.debug("error during the query")
            return False

        row = cur.safe_fetchone()
        if not row:
            logger.debug("error during the fetch")
            return False

        todo: SlackDB._SQLReturn = row[0]
        if todo == self._SQLReturn.BREAK:
            return True
        elif not (todo == self._SQLReturn.INSERT or todo == self._SQLReturn.UPDATE):
            logger.debug("bad result")
            return False

        user = {}
        profile = {}
        if userId:
            # Request the user info from the slack client.
            response = await self._unvexCall(self._slack.users_info, user=userId)
            if not response:
                logger.debug("Response was bad")
                return False

            # https://api.slack.com/types/user
            user: dict = response["user"]
            profile: dict = user.pop("profile")
        else:
            user["name"] = botInfo.get("name")

        userColumns = {
            users.keys.NAME: user.get("name"),
            users.keys.REALNAME: user.get("real_name"),
            users.keys.PROFILE_REALNAME: profile.get("real_name_normalized"),
            users.keys.PROFILE_DISPLAYNAME: profile.get("display_name_normalized"),
            users.keys.PROFILE_EMAIL: profile.get("email"),
            users.keys.ID: botId or userId,  # Needs to be last for the UPDATE's WHERE
        }
        userColumns = self._removeNulls(userColumns)

        if todo == self._SQLReturn.INSERT:
            (query, values) = Sql.basicInsert(users.NAME, userColumns)
        elif todo == self._SQLReturn.UPDATE:
            (query, values) = Sql.basicUpdate(users.NAME, userColumns)

        return cur.safe_execute(query, values)

    async def updateUsers(self, users: tuple, cur: SmartCursor | None = None) -> bool:
        if not cur:
            cur = SmartCursor(self._pool, self._dbname.as_string())
        uniqueUsers = set(users)
        for user in uniqueUsers:
            if not await self.updateUser(user, cur):
                return False
        return True

    async def updateChannel(
        self, channelid: str, cur: SmartCursor | None = None
    ) -> bool:
        """
        Insert channelid to channels.NAME, updating existing data with slack
        response if needed.
        """

        if not cur:
            cur = SmartCursor(self._pool, self._dbname.as_string())

        # Determinw what kind of action to take:
        #  1. INSERT if channelid isn't in the table.
        #  2. UPDATE if channelid is in the table, but stale.
        #  3. BREAK  if channelid is in the table, but not stale.
        query = Sql.SQL(
            "SELECT IF( NOT EXISTS ( SELECT 1 FROM {table} WHERE {chidkey}={chid} ), {ins}, IF( EXISTS ( SELECT 1 FROM {table} WHERE {chidkey}='{chid}' AND {lastupdate} < DATE_SUB( NOW(), INTERVAL {threshhold} SECOND ) ), {upd}, {brk} ) )"
        ).format(
            table=channels.NAME,
            chidkey=channels.keys.ID,
            chid=Sql.Placeholder("chid"),
            lastupdate=LASTUPDATE_KEY,
            threshhold=STALE_THRESHHOLD,
            ins=Sql.Literal(self._SQLReturn.INSERT),
            upd=Sql.Literal(self._SQLReturn.UPDATE),
            brk=Sql.Literal(self._SQLReturn.BREAK),
        )
        if not cur.safe_execute(query, {"chid": channelid}):
            logger.debug("error during the query")
            return False

        row = cur.safe_fetchone()
        if not row:
            logger.debug("error during the fetch")
            return False

        todo: SlackDB._SQLReturn = row[0]
        if todo == self._SQLReturn.BREAK:
            return True
        elif not (todo == self._SQLReturn.INSERT or todo == self._SQLReturn.UPDATE):
            logger.debug("bad result")
            return False

        # Request the user info from the slack client.
        response = await self._unvexCall(
            self._slack.conversations_info, channel=channelid
        )
        if not response:
            logger.debug("response was bad")
            return False

        # https://api.slack.com/types/conversation
        channel: dict = response["channel"]
        topic: dict = channel.setdefault("topic", {})
        purpose: dict = channel.setdefault("purpose", {})

        channelColumns = {
            channels.keys.NAME: channel["name"],
            channels.keys.CREATED: channel["created"],
            channels.keys.CREATOR: channel["creator"],
            channels.keys.ISPRIVATE: channel["is_private"],
            channels.keys.UPDATED: channel["updated"],
            channels.keys.TOPIC: topic.setdefault("value", ""),
            channels.keys.TOPIC_CREATOR: topic.setdefault("creator", ""),
            channels.keys.TOPIC_LAST_SET: topic.setdefault("last_set", 0),
            channels.keys.PURPOSE: purpose.setdefault("value", ""),
            channels.keys.PURPOSE_CREATOR: purpose.setdefault("creator", ""),
            channels.keys.PURPOSE_LAST_SET: purpose.setdefault("last_set", 0),
            channels.keys.ID: channelid,  # Needs to be last for the UPDATE's WHERE
        }
        channelColumns = self._removeNulls(channelColumns)

        if todo == self._SQLReturn.INSERT:
            (query, values) = Sql.basicInsert(channels.NAME, channelColumns)
        elif todo == self._SQLReturn.UPDATE:
            (query, values) = Sql.basicUpdate(channels.NAME, channelColumns)

        return await self.updateUsers(
            (channel["creator"], topic["creator"], purpose["creator"]), cur
        ) and cur.safe_execute(query, values)

    async def recordMessage(
        self, messageEvent: dict[str, Any], cur: SmartCursor | None = None
    ) -> bool:
        """
        Record a message event in the DB.
        """
        if not cur:
            cur = SmartCursor(self._pool, self._dbname.as_string())

        # blocks is an object made of other objects that we don't care to parse,
        # but do still want to record.
        if messageEvent.setdefault("blocks", None):
            # If it's non-null, convert to string
            blocks = repr(messageEvent["blocks"])
        else:
            blocks = "NULL"

        messageColumns = {
            messages.keys.TS: float(messageEvent["ts"]),
            messages.keys.USERID: messageEvent.get("user")
            or messageEvent.get("bot_id"),
            messages.keys.TEAM: messageEvent.get("team"),
            messages.keys.CHANNEL: messageEvent["channel"],
            messages.keys.THREAD_TS: float(messageEvent.setdefault("thread_ts", 0)),
            messages.keys.TEXT: messageEvent.setdefault("text", ""),
            messages.keys.BLOCKS: blocks,
        }
        messageColumns = self._removeNulls(messageColumns)
        (query, values) = Sql.basicInsert(messages.NAME, messageColumns)

        return (
            await self.updateUser(
                messageEvent.get("user") or messageEvent.get("bot_id"), cur
            )
            and await self.updateChannel(messageEvent["channel"], cur)
            and cur.safe_execute(query, values)
        )

    async def recordMessageDelete(
        self, event: dict[str, Any], userid: str | None, cur: SmartCursor | None = None
    ) -> bool:
        """
        Record a message deleted event in the DB.
        """
        if not cur:
            cur = SmartCursor(self._pool, self._dbname.as_string())

        # First see if we have the old message logged or not.
        #  1. INSERT if the message isn't in the table.
        #  2. UPDATE if the message is in the table and needs to be replaced.
        existsQuery = Sql.SQL(
            "SELECT IF( NOT EXISTS ( SELECT 1 FROM {table} WHERE {tskey}={ts} ), {ins}, {upd})"
        ).format(
            table=messages.NAME,
            tskey=messages.keys.TS,
            ts=Sql.Placeholder("ts"),
            ins=Sql.Literal(self._SQLReturn.INSERT),
            upd=Sql.Literal(self._SQLReturn.UPDATE),
        )
        if not cur.safe_execute(existsQuery, {"ts": float(event["deleted_ts"])}):
            logger.debug("error during the query")
            return False
        row = cur.safe_fetchone()
        if not row:
            logger.debug("error during the fetch")
            return False

        msgUserId = None
        if row[0] == self._SQLReturn.UPDATE:
            columns = {
                messages.keys.DELETED_AT: float(event["ts"]),
                messages.keys.DELETED_BY: userid,
                messages.keys.TS: float(event["deleted_ts"]),
            }
            (query, values) = Sql.basicUpdate(messages.NAME, columns)

        elif row[0] == self._SQLReturn.INSERT:
            message: dict[str, Any] = event["previous_message"]
            # blocks is an object made of other objects that we don't care to parse,
            # but do still want to record.
            if message.setdefault("blocks", None):
                # If it's non-null, convert to string
                blocks = repr(message["blocks"])
            else:
                blocks = "NULL"
            msgUserId = message.get("user") or message.get("bot_id")
            messageColumns = {
                messages.keys.USERID: msgUserId,
                messages.keys.TEAM: message.get("team"),
                messages.keys.CHANNEL: event["channel"],
                messages.keys.THREAD_TS: float(message.setdefault("thread_ts", 0)),
                messages.keys.TEXT: message.setdefault("text", ""),
                messages.keys.BLOCKS: blocks,
                messages.keys.DELETED_AT: float(event["ts"]),
                messages.keys.DELETED_BY: userid,
                messages.keys.TS: float(message["ts"]),
            }
            messageColumns = self._removeNulls(messageColumns)
            (query, values) = Sql.basicInsert(messages.NAME, messageColumns)

        else:
            logger.debug("bad return value")
            return False

        return (
            await self.updateUsers((userid, msgUserId), cur)
            and await self.updateChannel(event["channel"], cur)
            and cur.safe_execute(query, values)
        )

    async def recordMessageChanged(
        self, event: dict[str, Any], userid: str | None, cur: SmartCursor | None = None
    ) -> bool:
        """
        Record a message changed event in the DB.
        """

        if not cur:
            cur = SmartCursor(self._pool, self._dbname.as_string())

        message: dict = event.get("message", {})
        previous_message: dict = event.get("previous_message", {})

        # First see if we have the old message logged or not.
        #  1. INSERT if the message isn't in the table.
        #  2. UPDATE if the message is in the table and needs to be replaced.
        query = Sql.SQL(
            "SELECT IF( NOT EXISTS ( SELECT 1 FROM {table} WHERE {tskey}={ts} ), {ins}, {upd})"
        ).format(
            table=messages.NAME,
            tskey=messages.keys.TS,
            ts=Sql.Placeholder("ts"),
            ins=Sql.Literal(self._SQLReturn.INSERT),
            upd=Sql.Literal(self._SQLReturn.UPDATE),
        )
        if message and not cur.safe_execute(query, {"ts": message["ts"]}):
            logger.debug("error during the query")
            return False
        row = cur.safe_fetchone()
        if not row:
            logger.debug("error during the fetch")
            return False

        # blocks is an object made of other objects that we don't care to parse,
        # but do still want to record.
        if message.setdefault("blocks", None):
            # If it's non-null, convert to string
            blocks = repr(message["blocks"])
        else:
            blocks = "NULL"
        messageColumns = {
            messages.keys.USERID: message.get("user") or message.get("bot_id"),
            messages.keys.TEAM: message.get("team"),
            messages.keys.CHANNEL: event["channel"],
            messages.keys.THREAD_TS: float(message.setdefault("thread_ts", 0)),
            messages.keys.TEXT: message.setdefault("text", ""),
            messages.keys.BLOCKS: blocks,
            messages.keys.TS: float(
                message["ts"]
            ),  # Needs to be last for the UPDATE's WHERE
        }
        messageColumns = self._removeNulls(messageColumns)

        if row[0] == self._SQLReturn.INSERT:
            (messageQuery, messageValues) = Sql.basicInsert(
                messages.NAME, messageColumns
            )
        elif row[0] == self._SQLReturn.UPDATE:
            (messageQuery, messageValues) = Sql.basicUpdate(
                messages.NAME, messageColumns
            )
        else:
            logger.debug("bad return value")
            return False

        prev = {}
        prev["text"] = previous_message.get("text", "")
        prev["blocks"] = previous_message.get("blocks", {})

        changeLogColumns = {
            messagechanges.keys.CHANGED_AT: float(event["ts"]),
            messagechanges.keys.TS: float(event["message"]["ts"]),
            messagechanges.keys.CHANGED_BY: userid,
            messagechanges.keys.PREVIOUS_MESSAGE: repr(prev),
        }
        (changeLogQuery, changeLogValues) = Sql.basicInsert(
            messagechanges.NAME, changeLogColumns
        )

        return (
            await self.updateUsers(
                (userid, message.get("user") or message.get("bot_id")), cur
            )
            and await self.updateChannel(event["channel"], cur)
            and cur.safe_execute(messageQuery, messageValues)
            and cur.safe_execute(changeLogQuery, changeLogValues)
        )
