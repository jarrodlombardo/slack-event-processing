"""
This type stub file was generated by pyright.
"""

import mariadb

_DEFAULT_CHARSET = ...
_DEFAULT_COLLATION = ...
_MAX_TPC_XID_SIZE = ...
class Connection(mariadb._mariadb.connection):
    """
    MariaDB Connector/Python Connection Object

    Handles the connection to a MariaDB or MySQL database server.
    It encapsulates a database session.

    Connections are created using the method mariadb.connect()
    """
    def __init__(self, *args, **kwargs) -> None:
        """
        Establishes a connection to a database server and returns a connection
        object.
        """
        ...
    
    def cursor(self, cursorclass=..., **kwargs): # -> Cursor:
        """
        Returns a new cursor object for the current connection.

        If no cursorclass was specified, a cursor with default mariadb.Cursor
        class will be created.

        Optional keyword parameters:

        - buffered = True
          If set to False the result will be unbuffered, which means before
          executing another statement with the same connection the entire
          result set must be fetched.
          Please note that the default was False for MariaDB Connector/Python
          versions < 1.1.0.

        - dictionary = False
          Return fetch values as dictionary.

        - named_tuple = False
          Return fetch values as named tuple. This feature exists for
          compatibility reasons and should be avoided due to possible
          inconsistency.

        - cursor_type = CURSOR.NONE
          If cursor_type is set to CURSOR.READ_ONLY, a cursor is opened
          for the statement invoked with cursors execute() method.

        - prepared = False
          When set to True cursor will remain in prepared state after the first
          execute() method was called. Further calls to execute() method will
          ignore the sql statement.

        - binary = False
          Always execute statement in MariaDB client/server binary protocol.

        In versions prior to 1.1.0 results were unbuffered by default,
        which means before executing another statement with the same
        connection the entire result set must be fetched.

        fetch* methods of the cursor class by default return result set values
        as a tuple, unless named_tuple or dictionary was specified.
        The latter one exists for compatibility reasons and should be avoided
        due to possible inconsistency in case two or more fields in a result
        set have the same name.

        If cursor_type is set to CURSOR.READ_ONLY, a cursor is opened for
        the statement invoked with cursors execute() method.
        """
        ...
    
    def close(self): # -> None:
        ...
    
    def __enter__(self): # -> Self:
        ...
    
    def __exit__(self, exc_type, exc_val, exc_tb): # -> None:
        ...
    
    def commit(self): # -> None:
        """
        Commit any pending transaction to the database.
        """
        ...
    
    def rollback(self): # -> None:
        """
        Causes the database to roll back to the start of any pending
        transaction

        Closing a connection without committing the changes first will
        cause an implicit rollback to be performed.
        Note that rollback() will not work as expected if autocommit mode
        was set to True or the storage engine does not support transactions."
        """
        ...
    
    def kill(self, id: int): # -> None:
        """
        This function is used to ask the server to kill a database connection
        specified by the processid parameter.

        The connection id can be be retrieved by SHOW PROCESSLIST sql command.
        """
        ...
    
    def begin(self): # -> None:
        """
        Start a new transaction which can be committed by .commit() method,
        or cancelled by .rollback() method.
        """
        ...
    
    def select_db(self, new_db: str): # -> None:
        """
        Gets the default database for the current connection.

        The default database can also be obtained or changed by database
        attribute.
        """
        ...
    
    def get_server_version(self): # -> tuple[int, int, Any]:
        """
        Returns a tuple representing the version of the connected server in
        the following format: (MAJOR_VERSION, MINOR_VERSION, PATCH_VERSION)
        """
        ...
    
    def show_warnings(self): # -> None:
        """
        Shows error, warning and note messages from last executed command.
        """
        ...
    
    class xid(tuple):
        """
        xid(format_id: int, global_transaction_id: str, branch_qualifier: str)

        Creates a transaction ID object suitable for passing to the .tpc_*()
        methods of this connection.

        Parameters:

        - format_id: Format id. If not set default value `0` will be used.

        - global_transaction_id: Global transaction qualifier, which must be
          unique. The maximum length of the global transaction id is
          limited to 64 characters.

        - branch_qualifier: Branch qualifier which represents a local
          transaction identifier. The maximum length of the branch qualifier
          is limited to 64 characters.

        """
        def __new__(self, format_id, transaction_id, branch_qualifier): # -> Self:
            ...
        
    
    
    def tpc_begin(self, xid): # -> None:
        """
        Parameter:
          xid: xid object which was created by .xid() method of connection
               class

        Begins a TPC transaction with the given transaction ID xid.

        This method should be called outside of a transaction
        (i.e. nothing may have executed since the last .commit()
        or .rollback()).
        Furthermore, it is an error to call .commit() or .rollback() within
        the TPC transaction. A ProgrammingError is raised, if the application
        calls .commit() or .rollback() during an active TPC transaction.
        """
        ...
    
    def tpc_commit(self, xid=...): # -> None:
        """
        Optional parameter:"
        xid: xid object which was created by .xid() method of connection class.

        When called with no arguments, .tpc_commit() commits a TPC transaction
        previously prepared with .tpc_prepare().

        If .tpc_commit() is called prior to .tpc_prepare(), a single phase
        commit is performed. A transaction manager may choose to do this if
        only a single resource is participating in the global transaction.
        When called with a transaction ID xid, the database commits the given
        transaction. If an invalid transaction ID is provided,
        a ProgrammingError will be raised.
        This form should be called outside of a transaction, and
        is intended for use in recovery."
        """
        ...
    
    def tpc_prepare(self): # -> None:
        """
        Performs the first phase of a transaction started with .tpc_begin().
        A ProgrammingError will be raised if this method was called outside of
        a TPC transaction.

        After calling .tpc_prepare(), no statements can be executed until
        .tpc_commit() or .tpc_rollback() have been called.
        """
        ...
    
    def tpc_rollback(self, xid=...): # -> None:
        """
        Parameter:
           xid: xid object which was created by .xid() method of connection
                class

        Performs the first phase of a transaction started with .tpc_begin().
        A ProgrammingError will be raised if this method outside of a TPC
        transaction.

        After calling .tpc_prepare(), no statements can be executed until
        .tpc_commit() or .tpc_rollback() have been called.
        """
        ...
    
    def tpc_recover(self):
        """
        Returns a list of pending transaction IDs suitable for use with
        tpc_commit(xid) or .tpc_rollback(xid).
        """
        ...
    
    @property
    def database(self):
        """Get default database for connection."""
        ...
    
    @database.setter
    def database(self, schema): # -> None:
        """Set default database."""
        ...
    
    @property
    def user(self):
        """
        Returns the user name for the current connection or empty
        string if it can't be determined, e.g. when using socket
        authentication.
        """
        ...
    
    @property
    def character_set(self): # -> Literal['utf8mb4']:
        """
        Client character set.

        For MariaDB Connector/Python it is always utf8mb4.
        """
        ...
    
    @property
    def client_capabilities(self):
        """Client capability flags."""
        ...
    
    @property
    def server_capabilities(self):
        """Server capability flags."""
        ...
    
    @property
    def extended_server_capabilities(self):
        """
        Extended server capability flags (only for MariaDB
        database servers).
        """
        ...
    
    @property
    def server_port(self):
        """
        Database server TCP/IP port. This value will be 0 in case of a unix
        socket connection.
        """
        ...
    
    @property
    def unix_socket(self):
        """Unix socket name."""
        ...
    
    @property
    def server_name(self):
        """Name or IP address of database server."""
        ...
    
    @property
    def collation(self): # -> Literal['utf8mb4_general_ci']:
        """Client character set collation"""
        ...
    
    @property
    def server_info(self):
        """Server version in alphanumerical format (str)"""
        ...
    
    @property
    def tls_cipher(self):
        """TLS cipher suite if a secure connection is used."""
        ...
    
    @property
    def tls_version(self):
        """TLS protocol version if a secure connection is used."""
        ...
    
    @property
    def server_status(self):
        """
        Return server status flags
        """
        ...
    
    @property
    def server_version(self):
        """
        Server version in numerical format.

        The form of the version number is
        VERSION_MAJOR * 10000 + VERSION_MINOR * 100 + VERSION_PATCH
        """
        ...
    
    @property
    def server_version_info(self): # -> tuple[int, int, Any]:
        """
        Returns numeric version of connected database server in tuple format.
        """
        ...
    
    @property
    def autocommit(self): # -> bool:
        """
        Toggles autocommit mode on or off for the current database connection.

        Autocommit mode only affects operations on transactional table types.
        Be aware that rollback() will not work, if autocommit mode was switched
        on.

        By default autocommit mode is set to False."
        """
        ...
    
    @autocommit.setter
    def autocommit(self, mode): # -> None:
        ...
    
    @property
    def socket(self): # -> socket:
        """Returns the socket used for database connection"""
        ...
    
    @property
    def open(self): # -> bool:
        """
        Returns true if the connection is alive.

        A ping command will be send to the server for this purpose,
        which means this function might fail if there are still
        non processed pending result sets.
        """
        ...
    
    character_set_name = ...
    @property
    def thread_id(self):
        """
        Alias for connection_id
        """
        ...
    


