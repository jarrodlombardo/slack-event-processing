import logging
import os
from sys import argv
from typing import Callable

from slack_bolt import BoltContext
from slack_bolt.adapter.socket_mode.async_handler import AsyncSocketModeHandler
from slack_bolt.async_app import AsyncAck, AsyncApp, AsyncSay
from slack_sdk.web.async_client import AsyncWebClient

from slackdb import SlackDB

logging.basicConfig(level=logging.WARN, filename="slack-event-processing.log")

app = AsyncApp(token=os.environ["SLACK_BOT_TOKEN"])

dbkwargs = {}
dbkwargs["slackClient"]=app.client
dbkwargs["pool_name"]="slack"
dbkwargs["ssl"]=True

try:
    dbuser=os.environ["MARIADB_USER"]
except KeyError:
    dbuser = None
if dbuser:
    dbkwargs["user"] = dbuser

try:
    dbpw=os.environ["MARIADB_PASSWORD"]
except KeyError:
    dbpw = None
if dbpw:
    dbkwargs["password"] = dbpw

try:
    dbhost=os.environ["MARIADB_HOST"]
except KeyError:
    dbhost = None
if dbhost:
    dbkwargs["host"] = dbhost

# Use any kwargs from the commandline and put them in the dbkwargs.
# CLI kwargs override any already set above.
for kw in [ar.split('=', 1) for ar in argv if ar.find('=')>0]:
    kw1cf = kw[1].casefold()
    if kw1cf == "false":
        dbkwargs[kw[0]]=False
    elif kw1cf == "true":
        dbkwargs[kw[0]]=True
    else:
        dbkwargs[kw[0]]=kw[1]

db = SlackDB(**dbkwargs)

# Build the app's home screen
@app.event("app_home_opened")
async def update_home_tab(client, event, logger):
    try:
        # views.publish is the method that your app uses to push a view to the Home tab
        await client.views_publish(
            # the user that opened your app's app home
            user_id=event["user"],
            # the view object that appears in the app home
            view={
                "type": "home",
                "callback_id": "home_view",
                # body of the view
                "blocks": [
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": "This Bot listens to messages in the channels it is part of and processes them in various ways.",
                        },
                    },
                    {"type": "divider"},
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": "(Work in progress) The only processing supported so far is backsup to our DB on Linode.",
                        },
                    },
                ],
            },
        )

    except Exception as e:
        logger.error(f"Error publishing home tab: {e}")

@app.middleware
async def log_request(logger: logging.Logger, body: dict, next: Callable):
    """
    Log ~everthing to the debug logger.
    """
    logger.debug(body)
    return await next()

async def reply_in_thread(body: dict, say: AsyncSay, text: str):
    """
    Helper function to handle threaded bot replies.
    """
    event = body["event"]
    thread_ts = event.get("thread_ts", None) or event["ts"]
    await say(text, thread_ts=thread_ts)

# Left as an example on how to respond to a message.
# @app.message("backup")
# async def mention_backup(body: dict, say: AsyncSay):
#     """
#     Cute reply when people say "backup".
#     """
#     await reply_in_thread(body, say, "I'm listening to this channel and trying to back everything up.")

async def extract_subtype(body: dict, context: BoltContext, next: Callable):
    context["subtype"] = body.get("event", {}).get("subtype", None)
    await next()


@app.event(event={"type": "message", "subtype": "message_deleted"},)
async def onMessageDeleted(body: dict, context: BoltContext, logger: logging.Logger):
    text = body["event"]["previous_message"]
    logger.info(f" Deleted: {text}")
    async with asyncio.TaskGroup() as tg:
        tg.create_task(
            db.recordMessageDelete(body["event"], context.actor_user_id)
        ).add_done_callback(SlackDB.checkResultCallback)


@app.event(
    event={"type": "message", "subtype": "message_changed"},
)
async def onMessageChanged(body: dict, context: BoltContext, logger: logging.Logger):
    logger.info(f"\n\tprevious: {body["event"]["previous_message"]}\n\tcurrent: {body["event"]["message"]}")
    async with asyncio.TaskGroup() as tg:
        tg.create_task(
            db.recordMessageChanged(body["event"], context.actor_user_id)
        ).add_done_callback(SlackDB.checkResultCallback)


# TODO: Do something smart about file sharing messages
# @app.event(
#     event={"type": "message", "subtype": "file_share"}, middleware=[extract_subtype]
# )
# async def handle_file_share_messages(
#     body: dict, client: AsyncWebClient, context: BoltContext, logger: logging.Logger
# ):
#     subtype = context["subtype"]  # by extract_subtype
#     logger.info(f"subtype: {subtype}")
#     message_ts = body["event"]["ts"]
#     api_response = client.reactions_add(
#         channel=context.channel_id,
#         timestamp=message_ts,
#         name="eyes",
#     ) if context.channel_id else None
#     logger.info(f"api_response: {api_response}")

# TODO: Do something smart about file sharing
# @app.event("file_shared")
# async def handle_file_shared_events(body, logger):
#     logger.info(body)

@app.event("member_joined_channel")
async def handle_member_joined_channel_events(body, logger):
    logger.info(body)
    async with asyncio.TaskGroup() as tg:
        tg.create_task(db.updateChannel(body["event"]['channel'])).add_done_callback(SlackDB.checkResultCallback)

@app.event("group_left")
async def handle_group_left_events(body, logger):
    logger.warn(body)

@app.event("channel_left")
async def handle_channel_left_events(body, logger):
    logger.warn(body)

# This listener handles all uncaught message events
# The position in source code matters; this should be the last listener.
@app.event(event={"type": "message"}, middleware=[extract_subtype])
async def onMessageUnhandled(logger, context, body):
    subtype = context["subtype"]  # by extract_subtype
    logger.info(f"{subtype} is ignored")
    async with asyncio.TaskGroup() as tg:
        tg.create_task(db.recordMessage(body["event"])).add_done_callback(SlackDB.checkResultCallback)


async def main():
    handler = AsyncSocketModeHandler(app, os.environ["SLACK_APP_TOKEN"])
    await handler.start_async()

if __name__ == "__main__":
    import asyncio
    asyncio.run(main())
